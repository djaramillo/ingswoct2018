/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ingsoftwaregitlabexample;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Andres
 */
public class IngSoftwareGitLabExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Mostramos mensajes al usuario
        //y solicitamos ingrese los datos solicitados
        System.out.println("Registro de usuario: >");
        Scanner scan = new Scanner(System.in); 
        
        System.out.println("Nombre:>");
        String NombreUsuario = scan.nextLine();
        System.out.println("Telefono:>");
        String NumeroTelefono = scan.next();
        System.out.println("Año de nacimiento:>");
        String AnoNacimiento = scan.next();
        
        System.out.println("Guardando sus datos...");  
        System.out.println("Edad:"+CalcularEdad(AnoNacimiento));
        //Usamos el siguiente metodo para generar el fichero.
        //El metodo solicita los siguientes parametros.
        // Nombre, telefono, año de nacimiento
        GuardarDatos(NombreUsuario,NumeroTelefono,AnoNacimiento);
    }
    
    public static void GuardarDatos(String nombre, String telefono, String FechaNacimiento)
    {
        try //bloque try posibles errores al escribir el fichero
        {
            //indicamos el path del fichero.
            String path = String.format("src/%s.txt",nombre);
            File archivo = new File(path);            
            
            BufferedWriter bw = new BufferedWriter(new FileWriter(archivo));
            //Escribimos en el fichero los datos del usuario
            bw.write(String.format(" Nombre: %s%n Telefono: %s%n Fecha de nacimiento: %s%n",nombre,telefono,FechaNacimiento ));
            bw.close();
            //mensaje de la ruta de donde se genero el archivo
            System.out.println("Archivo guardado!:"+archivo.getAbsoluteFile());
            
        }catch(IOException ex)
        {
           System.out.println(ex.getMessage());
        }                
    }      
    
    public static String CalcularEdad(String edad)
    {        
        int resultado = 2018-Integer.parseInt(edad);
        return String.valueOf(resultado);
    }
}
